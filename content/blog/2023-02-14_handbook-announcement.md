---
date: 2023-02-14
title: 'KDE Eco Handbook: "Applying The Blue Angel Criteria To Free Software"'
categories: [Sustainability, Blauer Engel, Eco-Certification, Okular, KDE Eco, Community Lab]
author: Joseph P. De Veaugh-Geiss
summary: 'KDE Eco is proud to announce the publication of the first edition of the measurement handbook "Applying The Blue Angel Criteria To Free Software: A Handbook To Certify Software As Sustainable".'
SPDX-License-Identifier: CC-BY-SA-4.0
authors:
- SPDX-FileCopyrightText: 2023 Joseph P. De Veaugh-Geiss <joseph@kde.org>
---

Today is ["I ❤ Free Software!"](https://fsfe.org/activities/ilovefs/index.en.html) day and KDE Eco is proud to announce the publication of the first edition of the measurement handbook "Applying The Blue Angel Criteria To Free Software: A Handbook To Certify Software As Sustainable".

You can view the handbook at [our website](https://eco.kde.org/handbook/), where you can also download the latest PDF release for offline reading or for sharing with a friend or colleague.

{{< container class="text-center" >}}

![Cover image for the KDE Eco handbook (design by Lana Lutz).](/blog/images/cover.webp)

{{< /container >}}

The online version of the handbook is a living document that will undergo continuous updates. Have you measured the energy consumption of your software project? Want to add information to the handbook that is missing? Your contributions are very welcome! Be in touch or make a pull request at the [*Blauer Engel For FOSS*](https://invent.kde.org/teams/eco/be4foss/-/tree/master/handbook) (BE4FOSS) repository directly.

Perhaps you yourself are working on a cutting-edge method for measuring the energy consumption of software. Contributions to the [*FOSS Energy Efficiency Project*](https://invent.kde.org/teams/eco/feep) (FEEP) are also always welcome. While you're at it, take a look at KDE's [Sustainable Software Goal](https://invent.kde.org/teams/eco/sustainable-software-goal). There are numerous ways you can be a part of the movement to make Free & Open Source Software (FOSS) the most sustainable software.

{{< container class="text-center" >}}

![Left: comparing the global greenhouse gas emissions of the aviation industry at 2.5% (blue) with the ICT sector between 1.8% and 2.8% (green); right: showing projected estimates for emissions from the ICT sector (green) by 2050 if nothing changes. Data is from the Association Of Computing Machinery's [2021 Technology Policy Council](https://dl.acm.org/doi/pdf/10.1145/3483410) report. (Image from KDE published under a [CC-BY-SA-4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license. Design by Lana Lutz.)](/blog/images/sec1_acm-report.webp)

{{< /container >}}

FOSS communities are a powerful force for combatting the environmental impact of digitization. We have decades of experience organizing millions of contributors of all backgrounds from around the globe. Now it's a matter of turning plans into practice, goals into reality. Let's unite to combat software-driven environmental harm. Let's foster a culture of digital sustainability in our software communities. Let's build energy and resource efficient Free Software, together!

## Handbook Excerpt

Below is the introduction to the handbook. To read more, visit [our website](https://eco.kde.org/handbook/)!

> **Introduction: What's This All About?**
> 
> This handbook provides a brief overview of environmental harm driven by software, and how the Blue Angel ecolabel&mdash;the official environmental label of the German government&mdash;provides a benchmark for sustainable software design.
> 
> The Blue Angel is awarded to a range of products and services, from household cleaning agents to small appliances to construction products. In 2020, the German Environment Agency extended the award criteria to include software products. It was the first environmental certification in the world to link transparency and user autonomy&mdash;two pillars of Free & Open Source Software (FOSS)&mdash;with sustainability.
> 
> At this point, you might wonder: What does sustainability have to do with software at all? How can something so seemingly immaterial as software have an environmental footprint? In this handbook, we'll take a closer look at some of the ways software is contributing to the climate crisis, and how compliance with the Blue Angel award criteria for software eco-certification can help.
> 
> The book is divided into three parts:
> 
>  - Part I: Environmental Impact Of Software
>  - Part II: Eco-Certifying Desktop Software
>  - Part III: Fulfilling The Blue Angel Award Criteria
> 
> While Part I explores the *why* and Part II the *what* of software eco-certification, Part III discusses the *how* by explaining what you need to know to measure the energy consumption of your software and apply for the Blue Angel ecolabel. Specifically, in this section we provide a step-by-step guide to fullfilling the ABCs of the award criteria: (A) Resource & Energy Efficiency, (B) Potential Hardware Operating Life, and (C) User Autonomy.

View the Blue Angel award criteria for desktop software [here](https://www.blauer-engel.de/en/productworld/resources-and-energy-efficient-software-products).

{{< container class="text-center" >}}

![The ABCs of the Blue Angel award criteria for desktop software. (Image from KDE published under a [CC-BY-SA-4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.)](/blog/images/sec2_ABC-BE.webp)

{{< /container >}}

## Acknowledgments

Research and publications on software sustainability from the [Umwelt Campus Birkenfeld](https://www.umwelt-campus.de/en/green-software-engineering/refoplan-2018), [Öko-Institut](https://www.oeko.de/en/), and the [German Environment Agency](https://www.umweltbundesamt.de/en/the-uba) provide the foundation for this work. Tooling and documentation that made the handbook possible were provided by community members who have volunteered to contribute to this project for the benefit of all. Primary contributors include (listed in alphabetical order by first name): Arne Tarara, Cornelius Schumacher, Emmanuel Charruau, Karanjot Singh, Nicolas Fella, and Volker Krause. Thank you&mdash;your contributions make this handbook possible.

The text of the first version of the handbook was written and/or compiled from the above documentation by Joseph P. De Veaugh-Geiss. Olea Morris edited the text. Lana Lutz and Arwin Neil Baichoo made the book and website design as well as the images therein beautiful. Paul Brown made significant improvements to the Okular blog post which was adapted in Part II of the handbook. Wikipedia was a source of several texts which were included here in modified form: thank you to the community of Wikipedia writers and editors for making such a wonderful resource for all of us.

## Get Involved

Community is at the heart of the work of KDE and KDE Eco. Without you none of this would be possible!

Did you know? Discussions on software sustainability occur monthly at our community meetups on the 2nd Wednesday of the month from 19h to 20h (CET/CEST). [Join us](https://eco.kde.org/get-involved/). We would love to see you there!

And if you cannot make it to the monthly meetups, there are many other ways to [join the conversation](https://eco.kde.org/get-involved/) and help make Free Software sustainable software.

KDE Eco is supported by KDE e.V. as well as [BMUV](www.bmuv.de)/[UBA](https://www.umweltbundesamt.de/en), who fund the BE4FOSS project.

KDE is able to do this excellent work thanks to contributions from the community members, donors, and corporations that support us. Every individual counts, and every commitment, large or small, is a commitment to Free Software. Please consider heading to [KDE's fundraiser page and donating now](https://kde.org/community/donations/).

You can also follow us on Mastodon: <https://floss.social/@be4foss>

### Funding Notice

The BE4FOSS project was funded by the Federal Environment Agency and the Federal Ministry for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection (BMUV). The funds are made available by resolution of the German Bundestag.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="BMUV logo" width="340px"/>

<img src="/blog/images/uba.jpg" alt="UBA logo" width="250px"/>

{{< /container >}}

The publisher is responsible for the content of this publication.

